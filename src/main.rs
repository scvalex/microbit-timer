#![no_main]
#![no_std]

use cortex_m_rt::entry;
use derive_more::From;
use panic_rtt_target as _;
use rtt_target::{rprintln, rtt_init_print};

#[entry]
fn main() -> ! {
    rtt_init_print!();
    match run() {
        Err(err) => {
            rprintln!("FATAL ERROR: {:?}", err)
        }
        Ok(()) => {
            rprintln!("PROGRAM ENDED")
        }
    }
    #[allow(clippy::empty_loop)]
    loop {}
}

fn run() -> Result<(), Error> {
    let board = microbit::Board::take().ok_or(Error::BoardTake)?;
    let mut display = microbit::display::blocking::Display::new(board.display_pins);
    // 32768 Hz RTC / 2^12 = 8 ticks/sec
    let rtc = microbit::hal::rtc::Rtc::new(board.RTC0, 0b111111111111)?;
    rtc.enable_counter();
    let mut timer = microbit::hal::Timer::new(board.TIMER0);
    let mut screen: [[u8; 5]; 5] = [[0; 5]; 5];
    loop {
        let time = rtc.get_counter() / 8;
        // Show the days as a 5bit number on the first row
        let days = time / 60 / 60 / 24;
        for i in 0..5 {
            screen[0][5 - i - 1] = if days & (1 << i) != 0 { 1 } else { 0 };
        }
        // Show the hours as a 5bit number on the second row
        let hours = (time / 60 / 60) % 24;
        for i in 0..5 {
            screen[1][5 - i - 1] = if hours & (1 << i) != 0 { 1 } else { 0 };
        }
        // Show the minutes as a 10bit number on the third and fourth rows
        let minutes = (time / 60) % 60;
        for i in 5..10 {
            screen[2][10 - i - 1] = if minutes & (1 << i) != 0 { 1 } else { 0 };
        }
        for i in 0..5 {
            screen[3][5 - i - 1] = if minutes & (1 << i) != 0 { 1 } else { 0 };
        }
        // Show a progress bar on the last row.  The lit LED just
        // moves right every second and loops around.
        let progress = (time % 5) as usize;
        for i in 0..5 {
            screen[4][i] = if i == progress { 1 } else { 0 };
        }
        display.show(&mut timer, screen, 10);
        // rprintln!("RTC: {}", rtc.get_counter() / 8);
    }
}

#[derive(Debug, From)]
enum Error {
    Fmt(core::fmt::Error),
    Rtc(microbit::hal::rtc::Error),
    BoardTake,
}
