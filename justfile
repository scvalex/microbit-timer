default:
	just --choose

# Build all the rust
build:
	cargo fmt --all -- --check
	cargo build --release
	# exa -lh target/thumbv7em-none-eabihf/release/rust-x-microbit
	cargo size --release

# Build (debug) all the rust
debug:
	cargo build
	# exa -lh target/thumbv7em-none-eabihf/debug/rust-x-microbit
	cargo size

# Run the app natively
run:
	cargo embed

# Run the app natively in release mode
run-release:
	cargo embed --release

# Start gdb
gdb:
	arm-none-eabi-gdb --ex "target remote :1337" --ex "break main" --ex "layout src" target/thumbv7em-none-eabihf/debug/rust-x-microbit

# Run minicom
minicom:
	minicom -D /dev/ttyACM0 -b 115200 minirc.dfl

# Type-check and lint the code
clippy:
	cargo fmt --all -- --check
	cargo clippy --workspace

# Run Rust tests
test:
	cargo nextest run

# Clean everything
clean:
	cargo clean
